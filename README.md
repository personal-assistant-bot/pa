# Yet another personal assistant

Currently this is just a Telegram bot that doesn't understand anything
(and says so).

### Configuration

The application requires a file containing the telegram bot token. If
`TOKEN_FILE` environment variable is defined it should be the path to
token file. Otherwise use a `token.txt` file in the current working
directory.
